/* Show Frame Rates */
function setup() {
    const frameCount = createCanvas(400, 400);
    frameCount.parent(document.querySelector('#frameCount'));

    frameRate(10);
    textSize(30);
    textAlign(CENTER);
    init();
}
function draw() {
    background(200);
    text(frameCount, width / 2, height / 2);
}


