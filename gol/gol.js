const unitLength = 20;
const boxColor = 150; // Colour Picker --> input type="color"
const strokeColor = 50;
let columns; /* To be determined by window width */
let rows;    /* To be determined by window height */
let currentBoard;
let nextBoard;
let fr = 50; /* Frame Rate*/


function setup() {
    /* Set the canvas to be under the element #canvas*/
    const canvas = createCanvas(windowWidth, windowHeight - 250);
    canvas.parent(document.querySelector('#canvas'));

    frameRate(fr);

    /*Calculate the number of columns and rows */
    columns = floor(width / unitLength);
    rows = floor(height / unitLength);

    /*Making both currentBoard and nextBoard 2-dimensional matrix that has (columns * rows) boxes. */
    currentBoard = [];
    nextBoard = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = []
    }
    // Now both currentBoard and nextBoard are array of array of undefined values.
    init();  // Set the initial values of the currentBoard and nextBoard
}

/**
* Initialize/reset the board state
*/
function init() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = 0;
            nextBoard[i][j] = 0;
        }
    }
}

function draw() {
    background(255);
    generate();
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1) {
                fill(boxColor);
            } else {
                fill(255);
            }
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
}



function generate() {
    //Loop over every single box on the board
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            // Count all living members in the Moore neighborhood(8 boxes surrounding)
            let neighbors = 0;
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if (i == 0 && j == 0) {
                        // the cell itself is not its own neighbor
                        continue;
                    }
                    // The modulo operator is crucial for wrapping on the edge
                    neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
                }
            }

            // Rules of Life
            if (currentBoard[x][y] == 1 && neighbors < 2) {
                // Die of Loneliness
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 1 && neighbors > 3) {
                // Die of Overpopulation
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 0 && neighbors == 3) {
                // New life due to Reproduction
                nextBoard[x][y] = 1;
            } else {
                // Stasis
                nextBoard[x][y] = currentBoard[x][y];
            }
        }
    }
    console.log(currentBoard);
    // Swap the nextBoard to be the current Board
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}


/**
 * When mouse is dragged
 */
function mouseDragged() {
    /**
     * If the mouse coordinate is outside the board
     */
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);
    currentBoard[x][y] = 1;
    fill(boxColor);
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
    noLoop()
}



/**
 * When mouse is pressed
 */
function mousePressed() {
    noLoop();
    mouseDragged();
}

/**
 * When mouse is released
 */
function mouseReleased() {
    loop();

    // if (noLoop()) {
    //     loop();
    // }

}

/** Resize Window */
// function windowResized() {
//     resizeCanvas(windowWidth, windowHeight);
// }

// let width = document.querySelector('#windowWidth').value
// let height = document.querySelector('#windowHeight').value





/** START GAME  */
document.querySelector('#start-game')
    .addEventListener('click', function () {
        loop()
    })

/** PAUSE GAME */
document.querySelector('#pause-game')
    .addEventListener('click', function () {
        noLoop();
    });

/* RESET GAME */
document.querySelector('#reset-game')
    .addEventListener('click', function () {
        init();
    });



/* FRAME BY FRAME */
function frameSingle() {
    loop();
    noLoop();
}

document.querySelector('#frameSingle')
    .addEventListener('click', function () {
        frameSingle()
    })



/* POPULAR PATTERNS */
// function initGlider() {
//     for (let i = 0; i < columns; i++) {
//         for (let j = 0; j < rows; j++) {
//             currentBoard[i][j] = 0;

//             nextBoard[i][j] = 0;
//         }
//     }
//     noLoop(); //Set to Pause
// }

function initGlider() {
    for (let i = 0; i < 5; i++) {
        for (let j = 0; j < 5; j++) {
            currentBoard[i][j] = 1

            nextBoard[i][j] = 0;
        }
    }
    noLoop(); //Set to Pause
}


document.querySelector('#glider')
    .addEventListener('click', function () {
        initGlider()
    })


/* RANDOM PATTERN */
function initRandomPattern() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = random() > 0.8 ? 1 : 0;
            nextBoard[i][j] = 0;
        }
    }
    noLoop(); //Set to Pause
}

document.querySelector('#random')
    .addEventListener('click', function () {
        initRandomPattern()
    })


/* FPS Selector */

function FPSSelector() {
    let fr = document.querySelector('#fps-selector').value
    fr = parseInt(fr)
    frameRate(fr)
    console.log(typeof fr)
}

document.querySelector('#fps-selector')
    .addEventListener('input', FPSSelector)
let slider = document.getElementById('fps-selector')
let output = document.getElementById('slider-text')
output.innerHTML = "Current FPS: " + slider.value

slider.oninput = function () {
    output.innerHTML = "Current FPS: " + this.value
}